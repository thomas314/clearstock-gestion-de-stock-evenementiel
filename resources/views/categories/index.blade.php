@extends('layouts.app')

@section('content')
@auth 

<a class="btn btn-danger" href="{{ route('categories.create') }}"><i class="fas fa-plus-circle">Ajouter une catégorie</i></a>
<h2 class="text-danger mt-4">Liste des catégories existantes: </h2>
<table class="table table-bordered mt-3">
    <tr>
        <th class="text-danger">No</th>
        <th class="text-danger">Nom</th>
        <th class="text-danger">Action</th>
    </tr>
    @foreach ($categories as $categorie)
    <tr>
        <td>{{ $categorie->id }}</td>
        <td>{{ $categorie->name }}</td>
        <td>
            <form action="{{ route('categories.destroy',$categorie->id) }}" method="POST">

                <a class="btn btn-danger" href="{{ route('categories.edit',$categorie->id) }}">Editer</a>

                @csrf
                @method('DELETE')
  
                <button type="submit" class="btn btn-danger">Suppr.</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $categories->links() !!}
@endauth
@endsection