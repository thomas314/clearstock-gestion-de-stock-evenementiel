@extends('layouts.app')

@section('content')

    @if ($message = Session::get('info'))
        <div class="alert alert-info">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if (isset($details))
    <div class="container">
    <p class="text-danger">Voici la liste associée à votre recherche:</p>
    <a class="btn btn-danger mt-3" href="{{ route('elements.index') }}"> Retour</a>
    <table class="table table-bordered mt-3">
        <tr>
            <th class="text-danger">No</th>
            <th class="text-danger">Nom</th>
            <th class="text-danger">Résumé</th>
            <th class="text-danger">Quantité</th>
            <th class="text-danger">Catégorie</th>
        </tr>
        @foreach ($details as $element)
        <tr>
            <td>{{ $element->id }}</td>
            <td>{{ $element->name }}</td>
            <td>{{ $element->resume }}</td>
            <td>{{ $element->quantity }}</td>
            <td>{{ $element->category_id }}</td>
        </tr>
        @endforeach
    </table>
    @endif
    </div>

@endsection