@extends('layouts.app');

@section('content')
@if ($message = Session::get('info'))
        <div class="alert alert-info">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if (isset($details))
    <div class="container">
    <h2 class="text-danger">Voici la liste associée à votre recherche:</h2>
    <a class="btn btn-danger mb-3" href="{{ route('events.index') }}"> Retour</a>
    <table class="table table-bordered">
        <tr>
            <th class="text-danger">No</th>
            <th class="text-danger">Titre</th>
            <th class="text-danger">Résumé</th>
            <th class="text-danger">Catégorie</th>
            <th class="text-danger" width="280px">Action</th>
        </tr>
        @foreach ($details as $event)
        <tr>
            <td>{{ $event->id }}</td>
            <td>{{ $event->title }}</td>
            <td>{{ $event->resume }}</td>
            <td>
            @foreach ($cat as $category)
                @if($event->category_id == $category->id)
                    <span class="badge badge-danger">{{$category->name}}</span>
                @endif
            @endforeach
            </td>
            <td>
                <form action="{{ route('events.destroy',$event->id) }}" method="POST">
   
                    <a class="btn btn-danger" href="{{ route('events.show',$event->id) }}">Voir</a>
    
                    <a class="btn btn-danger" href="{{ route('events.edit',$event->id) }}">Editer</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Suppr.</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    @endif
    </div>
    @if(isset($elements))
    <div class="container">
    <h2 class="text-danger">Voici les éléments de mêmes catégories que les événements ci-dessus :</h2>
    <table class="table table-bordered">
        <tr>
            <th class="text-danger">No</th>
            <th class="text-danger">Nom</th>
            <th class="text-danger">Résumé</th>
            <th class="text-danger">Quantité</th>
            <th class="text-danger">Catégorie</th>
            <th class="text-danger" width="280px">Action</th>
        </tr>
        @foreach ($elements as $element)
        <tr>
            <td>{{ $element->id }}</td>
            <td>{{ $element->name }}</td>
            <td>{{ $element->resume }}</td>
            <td>{{ $element->quantity }}</td>
            <td>
            @foreach ($cat as $category)
                @if($element->category_id == $category->id)
                    <span class="badge badge-danger">{{$category->name}}</span>
                @endif
            @endforeach
            </td>
            <td>
                <form action="{{ route('elements.destroy',$element->id) }}" method="POST">
   
                    <a class="btn btn-danger" href="{{ route('elements.show',$element->id) }}">Voir</a>
    
                    <a class="btn btn-danger" href="{{ route('elements.edit',$element->id) }}">Editer</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Suppr.</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    @endif
</div>
@endsection