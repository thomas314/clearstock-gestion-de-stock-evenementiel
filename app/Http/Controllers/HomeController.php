<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Event;
use App\Element;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function findSearch(Request $request, Element $elements) {
        $request = $request->input('search');

        $findElement = Element::where('name', 'LIKE', '%'. $request .'%')->get();
        if(count($findElement) > 0){
            return view('elements.search', ["elements" => $elements])->withDetails($findElement)->withQuery($request);
        } else {
            return view('elements.search', ["elements" => $elements])->with('info','Aucun résultat ne correspond à votre recherche');
        }
    }
    public function categorize(Request $request, Element $element, Event $event) {
        $cat = Categories::all();
        $request = $request->input('category_id');

        $findElement = Element::where('category_id', '=', $request)->get();
        $findEvent = Event::where('category_id', '=', $request)->get();

        if(count($findElement) > 0 || count($findEvent) > 0) {
            return view('events.categorize', ['elements' => $element, 
                                              'events' => $event], compact('cat'))->withDetails($findEvent)->withElements($findElement)->withQuery($request);
        } else {
            return view('events.categorize', ['elements' => $element, 
                                              'events' => $event], compact('cat'))->with('info', 'Aucun résultat pour cette catégorie');
        }
        
    }
}
