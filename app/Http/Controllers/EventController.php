<?php

namespace App\Http\Controllers;

use App\Event;
use App\Categories;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::latest()->paginate(5);
        $categories = Categories::all();
  
        return view('events.index',compact('events', 'categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        return view('events.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $events = Event::create([
            'title' => $request->input('title'),
            'resume' => $request->input('resume'),
            'category_id' => $request->input('category_id')
        ]);
        $events->save();
        return redirect('/events')->with('success', 'Un nouvel événement vient d\'être ajouté ! ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        $categorie= Categories::all();
        return view('events.show', compact('event','categorie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $categories = Categories::all();
        return view('events.edit', compact('event','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $events = Event::whereId($id)->update([
            'title' => $request->input('title'),
            'resume' => $request->input('resume'),
            'category_id' => $request->input('category_id')
            
        ]);
        // $elements->save();
        return redirect('/events')->with('success', 'L\'événement a été modifié avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        return redirect('/events')->with('info', 'Cet événement a bien été supprimé de la liste');
    }
}
