<?php

namespace App\Http\Controllers;

use App\Element;
use App\Categories;
use Illuminate\Http\Request;

class ElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elements = Element::latest()->paginate(5);
        $categories = Categories::all();
  
        return view('elements.index',compact('elements', 'categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        return view('elements.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // je regarde si l'élement en Q° existe dans la BDD
        $findElement = Element::where('name', $request->input('name'))->first();
        // j'utilise la methode update ou create pour vérifier si l'entrée existe
        $elements = Element::updateOrCreate(
            // on vérifie si l'entrée existe
            ['name' => $request->input('name')],
            [
                // si elle existe ou non j'ajoute cela
                'name' => $request->input('name'),
                'resume' => $request->input('resume'),
                // si l'entrée n'existe pas je mets la quantité sinon j'additionne la quantité existante avec la quantité entrée par l'utilisateur
                'quantity' => $findElement == null ? $request->input('quantity') : $findElement->quantity + $request->input('quantity'),
                'category_id' => $request->input('category_id'),
                
            ]
        );

        $elements->save();
   
        return redirect('/elements')->with('success', 'Un nouvel élément a été ajouté à la liste ! ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $element = Element::findOrFail($id);
        $categorie= Categories::all();
        return view('elements.show', compact('element','categorie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $element = Element::findOrFail($id);
        $categories = Categories::all();
        return view('elements.edit', compact('element','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $elements = Element::whereId($id)->update([
            'name' => $request->input('name'),
            'resume' => $request->input('resume'),
            'category_id' => $request->input('category_id')
            
        ]);
        // $elements->save();
        return redirect('/elements')->with('success', 'L\'élément a été modifié avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $element = Element::findOrFail($id);
        $element->delete();

        return redirect('/elements')->with('info', 'Cet élément a bien été supprimé de la liste');
    }
}
