<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route Welcome
Route::get('/', function () {
    return view('welcome');
});
// Authentication
Auth::routes();
// Routes ressources du projet 
Route::resource('categories', 'CategoryController');
Route::resource('elements', 'ElementController');
Route::resource('events', 'EventController');
// Home du projet
Route::get('/home', 'HomeController@index')->name('home');
// Barres de recherche
Route::post('/findSearch', 'HomeController@findSearch');
Route::post('/categorize', 'HomeController@categorize');
